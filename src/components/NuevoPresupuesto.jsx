import React, { useState } from 'react'
import Mensaje from './Mensaje'

const NuevoPresupuesto = ({presupuesto,setPresupuesto,setIsValidPresupuesto}) =>{
    const [mensaje,setMensaje] = useState('')


    const handlePresupuesto =(e)=>{
        e.preventDefault();
        if(presupuesto<1){
            setMensaje("No es un presupuesto válido")
            return
        }
        console.log(presupuesto)
          setMensaje('');
          setIsValidPresupuesto(true);



    }
    return (
      <div
      className='contenedor-presupuesto contenedor sombra'
      >
        <form 
        onSubmit={handlePresupuesto}
        className='formulario'>
            <div className='campo'>
                <label>Definir Presupuesto</label>
                <input
                className='nuevopresupuesto'
                type="number"
                value={presupuesto}
                onChange={e=>setPresupuesto(Number(e.target.value))}
                placeholder="Añade tu Presupuesto"/>
            </div>
            <input type="submit" value="Añadir" />
            {mensaje && <Mensaje tipo="error">{mensaje}</Mensaje>}
        </form>
      
      </div>
    )
  }

export default NuevoPresupuesto